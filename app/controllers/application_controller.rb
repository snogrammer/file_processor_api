# frozen_string_literal: true

class ApplicationController < ActionController::API
  before_action :set_request_format
  respond_to :json, :csv

  def healthcheck
    render status: :ok, json: { status: 200, message: 'Welcome to the best file processor ever!' }
  end

  def raise_not_found!
    errors(["No route matches #{params[:unmatched_route]}"], :not_found)
  end

  protected

  def set_request_format
    request.format = :json
  end

  def errors(errors, status)
    @errors = errors
    render('common/error', status: status)
  end
end
