# frozen_string_literal: true

class CsvUploadController < ApplicationController
  before_action :validate_file_type

  def create
    file = csv_params[:file]
    @results = FileProcessor::Csv.process(file.path)

    respond_with @results, :created
  rescue StandardError => e
    Rails.logger.warn(e)

    error = ['File processing error occurred.']
    errors(error, 422)
  rescue InvalidContentTypeError => e
    Rails.logger.warn(e)

    error = ['Unsupported file type: CSV required.']
    errors(error, 415)
  end

  private

  def csv_params
    @csv_params ||= params.permit(:file)
  end

  def validate_file_type
    format = csv_params[:file].original_filename.split('.').last
    return if format.downcase.strip.eql?('csv')

    raise InvalidContentTypeError
  end

  class InvalidContentTypeError < StandardError; end
end
