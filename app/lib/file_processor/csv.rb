# frozen_string_literal: true

require 'csv'

module FileProcessor
  module Csv
    REVENUES = 'revenues'
    EXPENSES = 'expenses'

    module_function

    # @param file [String]
    # @return [Hash]
    def process(file)
      funds = {}

      CSV.foreach(file, headers: true) do |row|
        Rails.logger.debug(message: 'parsing rows', row: row)

        amount = begin
          value = row['Amount'].delete(',')
          BigDecimal(value)
        end
        type = amount.positive? ? REVENUES : EXPENSES
        year = row['Year']
        fund_name = row['Fund Name']
        department_name = row['Department Name']

        funds[year] ||= {}

        build_primary_fund_type(funds[year], type, 'funds', fund_name, amount)
        build_primary_fund_type(funds[year], type, 'departments', department_name, amount)

        funds[year][type]['total'] ||= 0.0
        funds[year][type]['total'] += amount
      end

      funds.sort_by { |key, _val| key }.reverse.to_h
    end

    # Hash builder helper
    #
    # @param fund_type [Hash]
    # @param type [Symbol]
    def build_primary_fund_type(fund_year, type, category, fund_name, amount)
      fund_year[type] ||= {}
      fund_year[type][category] ||= {}
      fund_year[type][category][fund_name] ||= 0.0
      fund_year[type][category][fund_name] += amount

      build_other_fund_type(fund_year, type, category, fund_name)
    end

    # Build other fund type
    #  Each hash needs to have corresponding type for revenues and expenses
    def build_other_fund_type(fund_year, type, category, fund_name)
      other_type = type.eql?(REVENUES) ? EXPENSES : REVENUES

      fund_year[other_type] ||= {}
      fund_year[other_type][category] ||= {}
      fund_year[other_type][category][fund_name] ||= 0.0
      fund_year[other_type]['total'] ||= 0.0
    end
  end
end
