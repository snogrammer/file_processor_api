# TODO: This can be DRYed up with view helpers.

json.aggregations do
  @results.each do |year, data|
    json.set! year do
      json.revenues do
        json.funds data['revenues']['funds'].sort_by { |k, v| k }.to_h.transform_values(&:to_f)
        json.departments data['revenues']['departments'].sort_by { |k, v| k }.to_h.transform_values(&:to_f)
        json.total data['revenues']['total'].to_f
      end

      json.expenses do
        json.funds data['expenses']['funds'].sort_by { |k, v| k }.to_h.transform_values(&:to_f)
        json.departments data['expenses']['departments'].sort_by { |k, v| k }.to_h.transform_values(&:to_f)
        json.total data['expenses']['total'].to_f
      end
    end
  end
end
