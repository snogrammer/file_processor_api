# frozen_string_literal: true

Rails.application.routes.draw do
  root 'application#healthcheck'

  post :scrub, to: 'csv_upload#create'

  # catch all invalid routes
  match '*unmatched_route', to: 'application#raise_not_found!', via: :all

  get :healthcheck, to: 'application#healthcheck'
end
