# frozen_string_literal: true

require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
# require "active_record/railtie"
# require "active_storage/engine"
require 'action_controller/railtie'
# require 'action_mailer/railtie'
require 'action_view/railtie'
# require 'action_cable/engine'
# require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module App
  module Config
    APP_NAME = 'checkr_api'
    MONGODB_URI = ENV.fetch('MONGODB_URI') { "mongodb://localhost:27017/file_processor_api_#{Rails.env}" }
    DB_MAX_CONNECTIONS = ENV.fetch('DB_MAX_CONNECTIONS') { 16 }
    DB_MIN_CONNECTIONS = ENV.fetch('DB_MIN_CONNECTIONS') { 5 }
    DB_WAIT_QUEUE_TIMEOUT = ENV.fetch('DB_WAIT_QUEUE_TIMEOUT') { 5 }
    DB_CONNECT_TIMEOUT = ENV.fetch('DB_CONNECT_TIMEOUT') { 10 }
    DB_SOCKET_TIMEOUT = ENV.fetch('DB_SOCKET_TIMEOUT') { 5 }
    REDIS_URL = ENV.fetch('REDIS_URL') { 'redis://localhost:6379/0' }
  end

  # Initialize configuration defaults for originally generated Rails version.
  class Application < Rails::Application
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    # In order for logs to function properly,
    # Docker expects your application or process to log to STDOUT
    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.log_tags  = %i[subdomain uuid]
    config.logger    = ActiveSupport::TaggedLogging.new(logger)

    # https://github.com/redis-store/redis-rails#rails-fragment-cache
    config.cache_store = :redis_store,
                         App::Config::REDIS_URL,
                         { expires_in: 90.minutes }
  end
end
