# frozen_string_literal: true

module Response
  module JsonHelpers
    def json
      @json ||= JSON.parse(response.body)
    end
  end
end
