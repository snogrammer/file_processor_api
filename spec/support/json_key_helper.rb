# frozen_string_literal: true

module JsonKeyHelper
  module_function

  def healthcheck
    %w[status message]
  end
end
