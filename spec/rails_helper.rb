# frozen_string_literal: true

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'
ENV['DEVISE_ORM'] = 'mongoid' # @see https://github.com/plataformatec/devise#devise_orm

require 'spec_helper'
require File.expand_path('../config/environment', __dir__)
abort('The Rails environment is running in production mode!') if Rails.env.production?

require 'rspec/rails'
require 'rake'
require 'json_spec'
require 'validates_email_format_of/rspec_matcher'

Dir[Rails.root.join('spec', 'support', '**', '*.rb')].each { |f| require f }

Rails.application.load_tasks

RSpec.configure do |config|
  config.include Rails.application.routes.url_helpers
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!

  config.include Response::JsonHelpers, type: :request
end
