# frozen_string_literal: true

source 'https://rubygems.org'

ruby '2.6.2'

gem 'bootsnap',          '>= 1.1.0', require: false

gem 'jbuilder',          '~> 2.7'
gem 'lograge',           '~> 0.11'
gem 'mongoid',           '~> 7.0'
gem 'mongoid_paranoia',  '~> 0.4'
gem 'multi_json',        '~> 1.13'
gem 'puma',              '~> 3.11'
gem 'rails',             '~> 5.2.3'
gem 'redis-rails',       '~> 5.0'
gem 'responders',        '~> 2.4'
gem 'sidekiq',           '~> 5.2'
gem 'validates_email_format_of',    '~> 1.6', '>= 1.6.3'
gem 'will_paginate_mongoid',        '~> 2.0'

group :development, :test do
  gem 'factory_bot_rails',  '~> 5.0'
  gem 'faker',              '~> 1.9'
  gem 'pry-rails'
  gem 'rspec-rails', '~> 3.8'
  gem 'rubocop'
  gem 'rubocop-rails'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring' # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'database_cleaner', '~> 1.7'
  gem 'json_spec',        '~> 1.1'
  gem 'mock_redis',       '~> 0.17'
  gem 'mongoid-rspec',    '~> 4.0'
  gem 'rspec-sidekiq',    '~> 3.0'
  gem 'shoulda-matchers', '~> 3.1'
  gem 'simplecov',        '~> 0.16'
end
