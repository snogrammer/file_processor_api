# https://pkgs.alpinelinux.org/packages?name=ruby&branch=v3.7
FROM ruby:2.6.2-alpine3.9

# Minimal requirements to run a Rails app
RUN apk add --no-cache --update build-base \
                                linux-headers \
                                nodejs \
                                tzdata \
                                git \
                                less \
                                chromium \
                                chromium-chromedriver

WORKDIR /app
COPY Gemfile* ./
RUN bundle install --jobs 5 --retry 5

# Copy the application into the container
COPY . .

# Precompile Rails assets
# RUN bundle exec rake assets:clean assets:precompile

# Start puma
CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]
