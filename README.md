# Project: File Processing API

---

### Overview

This project is meant to be a small, single-night kind of project that experienced developers will take ~1-2 hours and more junior developers ~4-5 hours. If you find yourself going past these time estimates, please pause and email us.

Our goal with this project is to see what your code looks like, how you structure things, and generally looking at your style. We want this to be as real-world as possible, so please feel free to use any tools or libraries you think would exemplify your general programming knowledge.

The FileAPI is a single endpoint API that takes in a POSTed CSV and returns a JSON package containing the rows parsed in the file and then some aggregates.

Please develop your code locally or in a private repo (BitBucket offers free private SVN/Git repos).

---

*Note: This project is a work in progress*

### Dependencies

This service has dependencies on the following services.

1. Mongo DB (Due to Heroku free-tier limitations with Postgres)
1. Redis

### Docker
Follow the Docker [Getting Started](https://docs.docker.com/mac/started/) guide to get `docker`, `docker-machine` and `docker-compose`.

1. Run the test suite: `$ script/test`

### Docker Compose
Docker compose is used for Jenkins and local development.  Use the information below to configure each environment.

##### Local development
To start the application using docker compose locally:

1. Start the services: `$ docker-compose up --build`
1. Access via browser: `$ open http://localhost:3000/`


### Testing

There is no need to write tests. We have a Heroku application that will post CSVs to your API endpoint, and give you a visual report of how your API responded. https://monkknows.herokuapp.com/

1. Test your code at https://monkknows.herokuapp.com/ -- Upon passing our test there will be instructions on next steps.

### Environment Variables

| Variable       |  Description   | Default                 |
| ---------------|:---------------|:-----------------------:|
| `APP_NAME`     | App name       | file_processor_api |
| `MONGODB_URI`  | Mongo DB URI   | mongodb://localhost:27017/file_processor_api_development |
| `REDIS_URL`  | Redis Url   | redis://localhost:6379/0 |
